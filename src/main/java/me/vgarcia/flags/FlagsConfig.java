package me.vgarcia.flags;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import me.vgarcia.flags.model.Continent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.util.List;

@Configuration
public class FlagsConfig {

  @Value("classpath:/continents.json")
  private Resource json;

  @Bean
  public ObjectMapper getObjectMapper() {
    return new ObjectMapper();
  }

  @Bean
  @SneakyThrows
  public List<Continent> getAllContinents(ObjectMapper objectMapper) {
    return objectMapper.readValue(json.getInputStream(), new TypeReference<List<Continent>>() {});
  }
}
