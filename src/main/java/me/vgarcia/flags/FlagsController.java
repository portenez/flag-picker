package me.vgarcia.flags;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.vgarcia.flags.model.Continent;
import me.vgarcia.flags.model.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@RequestMapping("/flags")
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FlagsController {

  private final FlagsRepository flagsRepository;

  @GetMapping("")
  Flux<Continent> getAllContinents() {
    return flagsRepository.getAll();
  }

  @GetMapping("/continents/{name}")
  Mono<ResponseEntity<Continent>> getContinent(@PathVariable("name") String name) {
    return flagsRepository
       .getByContinent(name)
       .map(ResponseEntity::ok)
       .defaultIfEmpty(ResponseEntity.notFound().build());
  }

  @GetMapping("/countries/{name}")
  Mono<ResponseEntity<Country>> getCountry(@PathVariable("name") String name) {
    return flagsRepository
       .getByCountry(name)
       .map(ResponseEntity::ok)
       .defaultIfEmpty(ResponseEntity.notFound().build());
  }

}
