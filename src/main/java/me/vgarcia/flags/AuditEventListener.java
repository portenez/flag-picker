package me.vgarcia.flags;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.audit.AuditEvent;
import org.springframework.boot.actuate.audit.AuditEventRepository;
import org.springframework.boot.actuate.audit.listener.AbstractAuditListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AuditEventListener extends AbstractAuditListener {

  @Autowired
  private AuditEventRepository auditEventRepository;

  @Override
  protected void onAuditEvent(AuditEvent event) {
    log.info("On audit event: timestamp: {}, principal: {}, type: {}, data: {}",
       event.getTimestamp(),
       event.getPrincipal(),
       event.getType(),
       event.getData()
    );
    auditEventRepository.add(event);
  }
}
