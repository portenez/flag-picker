package me.vgarcia.flags.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Continent {

  @JsonProperty("continent")
  String name;

  @Singular
  List<Country> countries;
}
