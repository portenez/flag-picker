package me.vgarcia.flags;

import io.micrometer.core.instrument.Metrics;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import me.vgarcia.flags.model.Continent;
import me.vgarcia.flags.model.Country;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Component
public class FlagsRepository {

  private final List<Continent> allContinents;
  private final Map<String, Continent> continentByName;
  private final Map<String, Country> countryIndex;

  FlagsRepository(List<Continent> allContinents) {
    this.allContinents = allContinents;
    continentByName =
        allContinents
           .stream()
           .collect(Collectors.toMap(Continent::getName, Function.identity()));

    countryIndex =
       allContinents
          .stream()
          .map(Continent::getCountries)
          .flatMap(List::stream)
          .collect(Collectors.toMap(Country::getName, Function.identity()));
  }

  Flux<Continent> getAll() {
    return Flux.fromIterable(allContinents);
  }

  Mono<Continent> getByContinent(String name) {
    val res = Optional.ofNullable(continentByName.get(name));
    res.ifPresent(x -> {
      Metrics.counter("me.vgarcia.continent", "name", name).increment();
    });
    return res.map(Mono::just).orElseGet(Mono::empty);
  }

  Mono<Country> getByCountry(String name) {
    val res = Optional.ofNullable(countryIndex.get(name));
    res.ifPresent(x -> {
      Metrics.counter("me.vgarcia.country", "name", name).increment();
    });
    return res.map(Mono::just).orElseGet(Mono::empty);
  }
}
