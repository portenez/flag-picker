package me.vgarcia.flags;

import me.vgarcia.flags.model.Country;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = Application.class)
public class CountryTest {

  @Rule
  public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

  private WebTestClient webTestClient;

  @Autowired
  ApplicationContext context;

  @Before
  public void setUp() {
    this.webTestClient = WebTestClient.bindToApplicationContext(context)
       .configureClient().baseUrl("http://localhost:8080")
       .filter(documentationConfiguration(restDocumentation))
       .build();
  }

	@Test
	public void australiaIsFound() {
    this.webTestClient
       .get()
       .uri("/flags/countries/{country}", "Australia")
       .exchange()
       .expectStatus().isOk()
       .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
       .expectBody(Country.class)
       .isEqualTo(new Country("Australia", "🇦🇺"))
       .consumeWith(
          document("countries",
             preprocessResponse(prettyPrint()),
             pathParameters(parameterWithName("country")
                .description("Capitalized name of the country"))
          )
       );
	}

	@Test
  public void notFoundCountry() {
    this.webTestClient
       .get()
       .uri("/flags/countries/Wakanda")
       .exchange()
       .expectStatus().isNotFound();
  }
}
