package me.vgarcia.flags;

import me.vgarcia.flags.model.Continent;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = Application.class)
public class FlagsTest {

  @Rule
  public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

  private WebTestClient webTestClient;

  @Autowired
  ApplicationContext context;

  @Before
  public void setUp() {
    this.webTestClient = WebTestClient.bindToApplicationContext(context)
       .configureClient().baseUrl("http://localhost:8080")
       .filter(documentationConfiguration(restDocumentation))
       .build();
  }

	@Test
	public void allFlags() {
    this.webTestClient
       .get()
       .uri("/flags")
       .exchange()
       .expectStatus()
       .isOk()
       .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
       .expectBodyList(Continent.class)
       .consumeWith(document("flags"));
	}

}
