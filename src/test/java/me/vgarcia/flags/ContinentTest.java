package me.vgarcia.flags;

import me.vgarcia.flags.model.Continent;
import me.vgarcia.flags.model.Country;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.document;
import static org.springframework.restdocs.webtestclient.WebTestClientRestDocumentation.documentationConfiguration;


@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = Application.class)
public class ContinentTest {

  @Rule
  public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

  private WebTestClient webTestClient;

  @Autowired
  ApplicationContext context;

  @Before
  public void setUp() {
    this.webTestClient = WebTestClient.bindToApplicationContext(context)
       .configureClient().baseUrl("http://localhost:8080")
       .filter(documentationConfiguration(restDocumentation))
       .build();
  }

	@Test
	public void africaIsFound() {
    this.webTestClient
       .get()
       .uri("/flags/continents/{continent}", "Africa")
       .exchange()
       .expectStatus().isOk()
       .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
       .expectBody(Continent.class)
       .isEqualTo(
          Continent.builder()
             .country(Country.builder().name("Nigeria").flag("🇳🇬").build())
             .country(Country.builder().name("Ethiopia").flag("🇳🇬").build())
             .country(Country.builder().name("Egypt").flag("🇪🇬").build())
             .country(Country.builder().name("DR Congo").flag("🇨🇩").build())
             .country(Country.builder().name("South Africa").flag("🇿🇦").build())
             .name("Africa")
             .build())
       .consumeWith(
          document("continents",
             preprocessResponse(prettyPrint()),
             pathParameters(parameterWithName("continent")
                .description("Capitalized name of the continent"))
          )
       );
	}

	@Test
  public void notFoundContinent() {
    this.webTestClient
       .get()
       .uri("/flags/continents/Atlantis")
       .exchange()
       .expectStatus().isNotFound();
  }
}
