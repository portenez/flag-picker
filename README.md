# Flags service

- [Flags service](#flags-service)
    - [Requirements](#requirements)
    - [TL;DR](#tldr)
        - [App Endpoints](#app-endpoints)
    - [Running](#running)
        - [Setup](#setup)
        - [Usage](#usage)
    - [Implementations details](#implementations-details)
    - [Metrics: Query counter](#metrics-query-counter)
    - [Dynamodb schema](#dynamodb-schema)
  

## Requirements

- java 1.8
- internet connection

## TL;DR

### App Endpoints

DOCS *GET* [http://localhost:8080]

All data *GET* [http://localhost:8080/flags] 

By Country *GET* `http://localhost:8080/flags/countries/{country}` eg: [http://localhost:8080/flags/countries/Mexico]

By Continent *GET* `http://localhost:8080/flags/continents/{continent}` eg: [http://localhost:8080/flags/continents/Europe]

Metrics both system and custom *GET* [http://localhost:8080/actuator/prometheus]
look for `vgarcia`



## Running

### Setup

Project should be setup via [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) included in the repo.

```bash
git clone https://gitlab.com/portenez/flag-picker.git
cd flag-picker
./gradlew bootRun
```

The compilation/app can also be run from within docker:

`docker run --rm -it -p 8080:8080 openjdk:8 /bin/bash`

Then run the script above (again here for convenience)

```bash
git clone https://gitlab.com/portenez/flag-picker.git
cd flag-picker
./gradlew bootRun
```

Then in another window you can go to `http:localhost:8080` for the docs

### Usage

Run the application and then go to `http:localhost:8080` for the documentation, 
including sample curls

## Implementations details

- [Lombok](https://projectlombok.org/) for code generation
- SpringBoot 2x
    - Spring 5x
        - Reactive RestWebFlux instead of blocking servlet
    - [Spring RestDocs](https://docs.spring.io/spring-restdocs/docs/2.0.2.RELEASE/reference/html5/#introduction) for automatic documentation (instead of swagger)
    - [Spring actuator](https://spring.io/guides/gs/actuator-service/) for production rediness
        - Health check
        - Logs endpoint
        - Performance metrics/monitoring
        - Custom application metrics. see [FlagsRepository](https://gitlab.com/portenez/flag-picker/blob/55880f884b7d7ab5b87c4b7499fb7b0d5d999891/src/main/java/me/vgarcia/flags/FlagsRepository.java#L48-48)
- self contained [gradle](https://gradle.org/) instead of maven
- Simple [MultiStage CI pipeline](https://gitlab.com/portenez/flag-picker/pipelines)

## Metrics: Query counter

I created a simple query counter via [micrometer](https://micrometer.io/docs), which is springs new metrics facade.

prometheus format at `http://localhost:8080/actuator/prometheus`

```
#...other metrics
# HELP me_vgarcia_continent_total  
# TYPE me_vgarcia_continent_total counter
me_vgarcia_continent_total{name="Africa",} 1.0
me_vgarcia_continent_total{name="America",} 1.0
me_vgarcia_continent_total{name="Europe",} 2.0
#...other metrics
# HELP me_vgarcia_country_total  
# TYPE me_vgarcia_country_total counter
me_vgarcia_country_total{name="Nigeria",} 1.0
me_vgarcia_country_total{name="Mexico",} 2.0
```



## Dynamodb schema

Can be represented with a simple dynamodb table

| Key | type | descr |
|-----|------|-------|
| country | primary key | hash for all the objects |
| continent | secondary global index (hash) | lookup hash |
| country   | secondary global index (sort) | compliments continent for uniqueness | 

primary index = country
seconday global index = continent

Dynamo is schemaless otherwise, so flag is not in the table description

```json
{
    "Table": {
        "AttributeDefinitions": [
            {
                "AttributeName": "continent",
                "AttributeType": "S"
            },
            {
                "AttributeName": "country",
                "AttributeType": "S"
            }
        ],
        "TableName": "Flags",
        "KeySchema": [
            {
                "AttributeName": "country",
                "KeyType": "HASH"
            }
        ],
        "TableStatus": "ACTIVE",
        "CreationDateTime": 1534580896.512,
        "ProvisionedThroughput": {
            "NumberOfDecreasesToday": 0,
            "ReadCapacityUnits": 1,
            "WriteCapacityUnits": 1
        },
        "TableSizeBytes": 0,
        "ItemCount": 0,
        "TableArn": "arn:aws:dynamodb:us-east-1:518406844771:table/Flags",
        "TableId": "cb8b091c-eafa-4a41-99e9-3cb761c1e4ae",
        "GlobalSecondaryIndexes": [
            {
                "IndexName": "continent-country-index",
                "KeySchema": [
                    {
                        "AttributeName": "continent",
                        "KeyType": "HASH"
                    },
                    {
                        "AttributeName": "country",
                        "KeyType": "RANGE"
                    }
                ],
                "Projection": {
                    "ProjectionType": "ALL"
                },
                "IndexStatus": "ACTIVE",
                "ProvisionedThroughput": {
                    "NumberOfDecreasesToday": 0,
                    "ReadCapacityUnits": 1,
                    "WriteCapacityUnits": 1
                },
                "IndexSizeBytes": 0,
                "ItemCount": 0,
                "IndexArn": "arn:aws:dynamodb:us-east-1:518406844771:table/Flags/index/continent-country-index"
            }
        ]
    }
}
```

